import os
import sys
import time
import logging
import threading as th
import multiprocessing as mp
from queue import Queue

import wired
from components import perf
from components import config as cfg
from components import report

# env setup

CURRENT = os.path.dirname(os.path.abspath(__file__))
PARENT = os.path.dirname(CURRENT)

# queues setup

perf_queue = mp.Queue()  # results from perf process
event_queue = Queue()  # button pressing events

# components setup

C = cfg.Config(os.path.join(CURRENT, 'config.yml'))

config = C.config
iface = config.iface

# wired setup

led = wired.LED(3)  # signal LED
btn_left = wired.BTN(pin=2,
                     queue=event_queue,
                     name='main')
btn_right = wired.BTN(pin=0,
                      queue=event_queue,
                      name='reset')
screen = wired.OLED(wired.device)

# starting button events emitters

btn_left.start()
btn_right.start()


class Application:
    """
        Main application class
    """

    shutdown = None
    shutdown_time = None
    running = False

    def __init__(self):
        self.start_time = int(time.time())
        self.current_perfs = dict()

    def update_shutdown(self, val):
        """
            Updates shutdown timer
        """
        self.shutdown_time = int(time.time()) + int(val)
        self.shutdown = None

    def clear_unwanted(self):
        """
            Empties queue from button events
        """
        while not event_queue.empty():
            if event_queue.get():
                pass

    def run_perf(self, perf):
        """
            Wrapper for receiving results from perf processes
        """
        result = None
        d = perf.direction
        self.current_perfs.update({d: perf})
        perf.start()

        while self.running:
            if not perf_queue.empty():
                msg = perf_queue.get()
                result = msg.get(d)
                if isinstance(result, tuple) and result[0] == 'exception':
                    screen.put(result[1])
                    raise Exception
                if result:
                    perf = self.current_perfs.pop(d)
                    perf.terminate()
                    return result
            else:
                time.sleep(.1)

        if not self.running:
            logging.debug('terminating tests...')
            if self.current_perfs:
                for p in self.current_perfs.values():
                    try:
                        p.terminate()
                    except Exception:
                        logging.exception(f'cannot terminate process {p}')
            raise SystemExit

    def check_reset(self):
        """
            Checks is it time for reset or shutdown or not
        """
        while self.running:
            if self.shutdown_time:
                # shutdown by timer
                if int(time.time()) > int(self.shutdown_time):
                    self.shutdown = True
                    self.running = None

            if btn_left.pressed():
                # reset device
                self.running = None
                self.shutdown = None
            else:
                time.sleep(.1)
                continue

    def start(self):
        """
            Start main loop and reset check loop
        """
        led.blink(2, .2)
        self.running = True
        self.update_shutdown(600)
        self.reset = th.Thread(target=self.check_reset,
                               daemon=True,
                               name='reset thread')
        self.reset.start()

        while self.running:
            self.run()
        else:
            self.stop()

    def run(self):
        """
            Application main routine
        """
        led.flash(.5)
        screen.put('')

        # waiting for link
        link_state = iface.link_state()
        if not link_state:
            screen.put(f'waiting link...'.upper())
            while not link_state:
                link_state = iface.retry(iface.link_state)
                if not self.running:
                    return
            else:
                screen.put(f'link up!')
                time.sleep(.1)

        try:
            screen.put('waiting DHCP...')
            iface.assign()
            while not iface.myaddr:
                iface.myaddr = iface.retry(iface.get_ip)
                if not self.running:
                    return
        except Exception:
            screen.put('no DHCP address\n'
                       f'device MAC is:\n{iface.mymac}')

        self.update_shutdown(600)

        led.stop()
        led.on()

        screen.put(f'{iface.myaddr}\n'
                   f'{iface.speed} {iface.duplex}\n'
                   'reset  |  start')

        while not btn_right.pressed():
            time.sleep(.1)
            if not self.running:
                return

        self.update_shutdown(600)

        try:
            # ready for perf tests
            led.flash(.1)
            msg = ''

            # preparing test processes
            _test = perf.PerfTest(config, perf_queue)
            _up = perf.PerfUp(config, perf_queue)
            _down = perf.PerfDown(config, perf_queue)

            # check connection by test perf
            screen.put('preparing test,\nplease, wait...')
            self.run_perf(_test)

            # download perf test
            msg += 'downloading...'
            screen.put(msg)
            download = self.run_perf(_down)
            if download:
                msg = f'd| {download.get("speed")}'
                screen.put(msg + '\nuploading...')

            # upload perf test
            upload = self.run_perf(_up)
            if upload:
                msg += f'\nu| {upload.get("speed")}'
            msg += '\npress any'
            screen.put(msg)

            # reports
            led.stop()
            led.on()
            final_report = report.Report(config, (download, upload), iface)
            screen.put('sending email...')
            self.clear_unwanted()  # clear mis-clicked left button

            # mailing report
            try:
                final_report.sendmail()
            except Exception:
                logging.exception('fail when sending mail...')
                screen.put('WARNING!\nmail send failed')
                time.sleep(2)
            self.clear_unwanted()  # clear mis-clicked left button
            screen.put(msg)
            self.update_shutdown(600)

            while not btn_right.pressed():
                time.sleep(.1)
                if not self.running:
                    return

        except (SystemExit, KeyboardInterrupt):
            led.stop()
            return
        except Exception:
            logging.exception('performance test failed')
            led.stop()
            led.flash(.8)
            while not btn_right.pressed():
                time.sleep(.1)
                if not self.running:
                    return

    def stop(self):
        """
            Stops current loop, reset or shutdown device
        """
        screen.put('resetting...')
        time.sleep(.2)
        try:
            # stopping listeners and leds
            led.stop()
            btn_left.stop()
            btn_right.stop()
            if self.shutdown:
                self.update_shutdown(600)
                self.power_off()
            else:
                # should be restarted by systemd
                raise SystemExit
        except (KeyboardInterrupt, SystemExit):
            logging.info('*** stopping application ***')
        except Exception:
            logging.exception('cannot stop application')

    def power_off(self):
        logging.warning('shutdown on timeout event catched '
                        f'at {int(time.time())} with start '
                        f'at {self.start_time}')
        # power off still in testing
        # p = subprocess.Popen('shutdown -h now',
        #                       stdout=subprocess.PIPE, shell=True)


if __name__ == '__main__':
    logging.info('*** service starting ***')
    logging.debug(f'start time: {int(time.time())}')
    app = Application()
    app.start()
    # if not running anymore
    logging.info('exiting...')
    sys.exit(1)
