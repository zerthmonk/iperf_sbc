#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import os.path
import threading as th

import time

from PIL import ImageFont

from luma.oled.device import ssd1306
from luma.core.interface.serial import i2c
from luma.core.virtual import viewport, snapshot

import wiringpi as wpi


def GPIO_enable():
    """
        Sets GPIO Pins to input
    """
    wpi.wiringPiSetup()  # !!
    _outer = [7, 0, 2, 3, 13, 14, 21, 24]
    _pwm = [12, 23]
    _inner = [1, 4, 5, 6, 10, 11, 26, 27]
    pins = _outer + _pwm + _inner

    for pin in pins:
        wpi.pinMode(pin, 1)
        wpi.digitalWrite(pin, 0)

    return pins


gpio = GPIO_enable()


class StoppableThread(th.Thread):
    """
        Thread with stop event
    """
    def __init__(self, *args, **kwargs):
        super(StoppableThread, self).__init__(*args, **kwargs)
        self._stop_event = th.Event()

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()


class PIN():
    """
        Basic pin operations
    """
    def __init__(self, pin, mode=1):
        if pin not in gpio:
            raise Exception(f'[!] no such pin {pin}')
        wpi.pinMode(pin, mode)
        self.pin = pin
        self.off()

    def on(self):
        wpi.digitalWrite(self.pin, 1)

    def off(self):
        wpi.digitalWrite(self.pin, 0)

    @property
    def status(self):
        return wpi.digitalRead(self.pin)


class LED(PIN):
    """
        Light emitting diode operations
    """
    def __init__(self, pin):
        super().__init__(pin)
        self.thread = None

    def switch(self, duration):
        self.on()
        time.sleep(duration)
        self.off()
        time.sleep(duration)

    def _f(self, duration):
        while self.running:
            self.switch(duration)

    def flash(self, duration):
        t = StoppableThread(target=self._f,
                            daemon=True,
                            args=(duration,))
        self.thread = t
        t.start()

    def blink(self, times, duration=.3):
        for x in range(times):
            self.switch(duration)

    def stop(self):
        if self.thread:
            self.thread.stop()
            self.thread.join(1)
        self.off()


class BTN(StoppableThread):
    """
        Button operations listener
    """
    def __init__(self, queue, pin, name='button'):
        super().__init__()
        self.pin = PIN(pin)
        self.name = name
        self.q = queue
        self.initial = self.pin.status

    def run(self):
        try:
            while not self.stopped():
                if self.pin.status != self.initial:
                    start_time = time.time()
                    while self.pin.status != self.initial:
                        time.sleep(.01)
                    press_time = round(time.time() - start_time, 2)
                    self.q.put({self.name: press_time})
                else:
                    time.sleep(.01)
        except (KeyboardInterrupt, SystemExit):
            raise

    def pressed(self, t=.05):
        try:
            if self.q.empty():
                return
            msg = self.q.get()
            if msg:
                if not msg.get(self.name):
                    self.q.put(msg)
                elif msg.get(self.name) >= t:
                    while not self.q.empty():
                        # clear unwanted events
                        self.q.get()
                    return True
        except Exception:
            pass


class OLED():
    """
        SSD1106 AMOLED screen operations
    """
    def __init__(self, device):
        self.show = None
        self.dev = device
        self.font = 'font_everyday.ttf'
        self.timeout = .6

    def put(self, text):
        """
            Puts text to screen

            This is recipe from SO
        """
        self.clear()
        if isinstance(text, (list, tuple)):
            text = "\n".join(text)
        fonts = [self._make_font(self.font, sz) for sz in range(24, 8, -2)]
        sq = self.dev.width * 2
        virtual = viewport(self.dev, sq, sq)
        widget = self._make_snapshot(self.dev.width, self.dev.height, text,
                                     fonts, "white")
        position = [6, 6]
        virtual.add_hotspot(widget, position)
        virtual.set_position((6, 6))
        virtual.remove_hotspot(widget, position)

    def _make_font(self, name, size):
        """
            Create ImageFont from TTF file
        """
        font_path = os.path.abspath(os.path.join(
            os.path.dirname(__file__), 'res', self.font))
        return ImageFont.truetype(font_path, size)

    def _make_snapshot(self, width, height, text, fonts, color="white"):
        """
            Render text for screen output

            This is recipe from SO
        """
        def render(draw, width, height):
            t = text

            for font in fonts:
                size = draw.multiline_textsize(t, font)
                if size[0] > width:
                    size = draw.multiline_textsize(t, font)
                else:
                    break

            left = (width - size[0]) // 2
            top = (height - size[1]) // 2
            draw.multiline_text((left, top), text=t, font=font, fill=color,
                                align="center", spacing=2)

        return snapshot(width, height, render, interval=10)


serial = i2c(port=1, address=0x3C)
device = ssd1306(serial, width=128, height=64)
device.contrast(1)
