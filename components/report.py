import time
import json
import logging
import socket
import smtplib

from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


class Report:
    """
        Make test statistics report and send by mail
    """

    def __init__(self, config, results, iface):
        self.results = results
        self.config = config
        self.name = socket.gethostname()
        self.addr = iface.myaddr
        self.mac = iface.mymac
        self.mailfrom = self.config.smtp.get('from', '')
        self.mailto = self.config.smtp.get('to', '')
        self.link = " ".join((iface.speed, iface.duplex))
        self.start = results[0].get('starttime', 'badtime')
        self.dur = sum([r["duration"] for r in self.results])

    def _plain(self):
        """
            Plain text report data formatter
        """
        res = ''
        for r in self.results:
            res += f'{r["speed"]:15s} {r["direction"]} speed\n'

        p = (
            f'{self.name} test result:\n'
            f'{"-" * 48}\n'
            f'{res}'
            f'{"-" * 48}\n'
            f'IP: {self.addr} | MAC: {self.mac}\n'
            f'test total duration {self.dur} sec'
        )
        return p

    def _html(self):
        """
            HTML report data formatter
        """
        res = ''
        for r in self.results:
            res += f'<b>{r["speed"]:15s}</b> {r["direction"]} speed <br>'

        h = (
            f'<html><head></head>'
            f'<body style="font-size: 1.25rem; letter-spacing: 1px;">'
            f'<p>{self.name} test result:<hr><p>'
            f'{res}'
            f'</p><hr><p style="font-size: 0.875rem">'
            f'IP: {self.addr} | MAC: {self.mac}</b></p>'
            f'<p style="font-size: 0.875rem">'
            f'test total duration {self.dur} sec'
            f'</body></html>'
        )
        return h

    def _temp(self):
        """
            Temperature sensor reader
        """
        with open('/sys/devices/virtual/thermal/thermal_zone0/temp',
                  'r') as temp:
            t = int(temp.read())
            return t / 1000

    def _json(self):
        """
            JSON report data formatter
        """
        res = dict()
        for r in self.results:
            d = r.get('direction', 'none').lower()
            speed = r.get('speed')
            if speed:
                r['speed'] = int(r.pop('speed_raw', 0))
            if r.get('starttime'):
                r.pop('starttime', None)
            if d.startswith(('up', 'down')):
                res[d] = r
            else:
                logging.error(f'unknown direction for {r}')

        try:
            data = {
                "hostname": self.name,
                "ip": self.addr,
                "mac": self.mac,
                "temp": self._temp(),
                "link": self.link,
                "time": int(time.time()),
            }
            # parsing iperf json
            data.update(res)
            logging.debug(json.dumps(data))
            return json.dumps(data)
        except Exception:
            raise

    def sendmail(self):
        """
            Send report by mail
        """
        message = MIMEMultipart()
        message['Subject'] = f'[perf] test completed from {self.addr}'
        message['From'] = self.mailfrom

        txt = MIMEMultipart('alternative')
        txt.attach(MIMEText(self._html(), 'html'))
        txt.attach(MIMEText(self._plain(), 'plain'))

        attachment = MIMEBase('application', 'json', charset='utf-8')
        attachment.set_payload(self._json(), charset='utf-8')
        attachment.add_header('Content-Disposition', 'attachment',
                              filename="results.json")

        message.attach(txt)
        message.attach(attachment)

        try:
            if isinstance(self.mailto, list):
                message['To'] = ', '.join(self.mailto)
            else:
                message['To'] = self.mailto
            with smtplib.SMTP(self.config.smtp['host']) as smtp:
                smtp.send_message(message, self.mailfrom, self.mailto)
            logging.info(f'report sent to {self.mailto}')
        except Exception:
            logging.exception(
                f'report failed {self.mailfrom} '
                f'to {self.mailto} as {self._plain()}')
            raise
