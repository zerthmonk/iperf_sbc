import yaml
import logging
from types import SimpleNamespace

from components.iface import Iface


class Config:
    """
        App configuration setup
    """

    config = dict()

    def __init__(self, confpath):
        with open(confpath) as cf:
            try:
                config = yaml.load(cf)
            except yaml.YAMLError:
                logging.exception('yaml error while loading config')

        # critical config parts check

        for k in ('iperf', 'smtp'):
            self._crit(config, k)
        for k in ('host', 'port'):
            self._crit(config['iperf'], k, 'iperf')
            self._crit(config['smtp'], k, 'smtp')

        self.config.update(config)

        self._create_log(self.config['misc']['logpath'])
        self._get_iface()
        self.config = SimpleNamespace(**self.config)

    def _create_log(self, fname):
        """
            Configures logger
        """

        FORMAT = '%(asctime)s :: <%(filename)s:%(lineno)s - %(funcName)s()>'\
                 '  %(levelname)s > %(message)s'
        level_name = self.config['misc'].get('loglevel', 'DEBUG')
        level = logging.getLevelName(level_name)

        logging.basicConfig(filename=fname,
                            level=level,
                            format=FORMAT)

    def _crit(self, d, key, dname='config'):
        if not d.get(key, None):
            raise KeyError(f"[!] {key} missing from {dname}")

    def _get_iface(self):
        """
            Defines main network interface
        """
        iface = Iface(self.config['misc'].get('iface', 'eth0'))
        self.config['iface'] = iface

    def __repr__(self):
        return '<Config> {}'.format(self.config)
