import os
import time
import fcntl
import logging
import sp as sp
import socket
import struct

# dhcp and MAC


def get_mac(ifname):
    """
        Get MAC address of network interface by name
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,
                       struct.pack('256s', bytes(ifname, 'utf-8')[:15]))
    return ':'.join('%02x' % b for b in info[18:24])


class Iface:
    """
        Network interface operations and statistics
    """

    MAC = '00:12:34:56:00:ff'  # test mac-address

    def __init__(self, interface):
        self.myaddr = None
        self.mymac = get_mac(interface)
        self.statpath = '/sys/class/net/'
        self.iface = interface
        try:
            self.link_state()
            self.get_ip()
        except Exception:
            pass

    def _release(self):
        """
            DHCP release
        """
        self.popen(f'dhclient -r', timeout=15)
        self.popen(f'ifconfig {self.iface} down')
        time.sleep(1)
        self.popen(f'ifconfig {self.iface} up')

    def retry(self, callback, sleep=1, tries=None):
        """
           Retry callback function

           Todos:
             make decorator from this function
        """
        res = None
        if not tries:
            while True:
                res = callback()
                if res:
                    logging.debug(f'{callback.__name__} return {res}')
                    break
                else:
                    logging.debug(f'retrying {callback.__name__}')
                time.sleep(sleep)
        else:
            if tries <= 0:
                raise Exception(f'{callback.__name__}'
                                'is failed to get any result')
            else:
                res = callback()

            if not res:
                time.sleep(sleep)
                return self.retry(tries - 1, callback)
        return res

    def link_state(self):
        """
            Get current network interface link state info
        """
        time.sleep(.3)
        with open(os.path.join(self.statpath, self.iface, 'carrier')) as fh:
            try:
                state = int(fh.read().rstrip())
                if state == 0:
                    raise  # just going to except block
                else:
                    logging.debug(f'{self.iface} link state: UP')
                    return True
            except Exception:
                # seems like '' and valueerror, or 0
                # anyway - link down and there's nothing we can do
                logging.debug(f'{self.iface} link state: DOWN')
                return

    def link_speed(self):
        """
            Get current network interface link speed info
        """
        time.sleep(.3)
        with open(os.path.join(self.statpath, self.iface, 'speed')) as fh:
            self.speed = fh.read().rstrip()
        with open(os.path.join(self.statpath, self.iface, 'duplex')) as fh:
            self.duplex = fh.read().rstrip().upper()
        logging.debug(f'link state (speed/duplex): '
                      f'{self.speed} / {self.duplex}')
        return (self.speed, self.duplex)

    def assign(self):
        """
            Get all needed network interface parameters
        """
        try:
            self.myaddr = self.get_ip()
            (speed, duplex) = self.link_speed()
            self.gwaddr = self.get_gw()
            logging.info('Network UP ({speed} {duplex})\n'
                         '\t\t\tME: {myaddr} {mymac} GW: {gwaddr}\n'
                         .format(**self.__dict__))  # excessive selfs
            return (self.myaddr, self.mymac, self.gwaddr,
                    self.speed, self.duplex)
        except Exception:
            logging.exception(f'bad assignment for {self}')

    def popen(self, cmd, timeout=5):
        """
            Custom process opener with logs
        """
        logging.debug(f"starting subprocess: {cmd}")
        p = sp.Popen(cmd, stdout=sp.PIPE, shell=True)
        try:
            stdout, stderr = p.communicate(timeout=timeout)
            return stdout
        except sp.TimeoutExpired:
            logging.exception('process timeout expired')
            p.kill()
            raise
        except Exception:
            logging.exception('subprocess failed:')
            p.kill()
            raise

    def get_gw(self):
        """
            Read the default gateway directly from /proc. SO by ssokolow
        """
        try:
            with open("/proc/net/route") as fh:
                for line in fh:
                    fields = line.strip().split()
                    if fields[1] != '00000000' or not int(fields[3], 16) & 2:
                        continue
                    return socket.inet_ntoa(
                        struct.pack("<L", int(fields[2], 16)))
        except Exception:
            logging.error('cannot get gateway address from /proc')
            return

    def get_ip(self):
        """
            Get IP address info. SO by ssokolow
        """
        addr = None
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            addr = socket.inet_ntoa(fcntl.ioctl(
                s.fileno(),
                0x8915,  # SIOCGIFADDR
                struct.pack('256s', bytes(self.iface[:15], 'utf-8'))
            )[20:24])
            if not addr:
                raise OSError
        except OSError:
            # ok, try with subprocess
            addr = sp.check_output(['hostname', '-I']).rstrip().decode()

        if addr and addr != '':
            logging.debug(f'get IP on {self.iface}: {addr}')
            return addr
