import iperf3
import socket
import logging
import multiprocessing as mp

# perf wrapper


class Perf(mp.Process):
    def __init__(self, config, queue):
        super().__init__()
        self.client = iperf3.Client()
        self.config = config.iperf
        self.queue = queue
        self.client.server_hostname = self.config['host']
        self.client.port = self.config['port']
        self.client.duration = self.config.get('duration', 20)
        self.client.protocol = self.config.get('protocol', 'tcp')
        self.client.zerocopy = self.config.get('zerocopy', True)
        # self.client.blksize = self.config.get('blksize', 102500000)
        self.client.num_streams = self.config.get('streams', 4)
        self.duration = self.client.duration

    def _err(self):
        c = " ".join([f'{k}: {v}' for (k, v) in self.client.__dict__.items()])
        raise RuntimeError(f'{c}')

    def check_remote(self):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((self.config['host'], int(self.config['port'])))
            s.shutdown(2)
            return True
        except Exception:
            logging.exception('cannot reach perf server')
            self.queue.put({self.direction: ('exception', f'server off\n'
                           f'{self.config["host"]}\n{self.config["port"]}')})

    def run(self):
        super().run()
        try:
            self.check_remote()
            logging.debug(f'run {self} client {self.client.__dict__}')
            self.result = self.client.run()
            cpuutil = round(self.result.local_cpu_total, 2)
            stats = {
                'direction': self.direction,
                'speed': self._speed_conv(self._bsum(self.result)),
                'speed_raw': self._bsum(self.result),
                'starttime': ' '.join(self.result.time.split(' ')[1:-1]),
                'duration': self.result.duration,
                'cpu': cpuutil
            }

            logging.info(
                'DONE {direction} {speed} {duration}s run'.format(**stats))
            logging.debug(f'Avg. CPU utilization: {cpuutil}')
            self.queue.put({self.direction: stats})
        except BaseException as e:
            return ({self.direction: ('exception', str(e))})

    def _speed_conv(self, bps):
        """
            Simple human readable speed output
            TestResult.sent_* methods seems insufficient

            >>> speed_readable(100)
            '100 bit/s'
            >>> speed_readable(1000)
            '1.0 Kbit/s'
            >>> speed_readable(1000000)
            '1.0 Mbit/s'
        """
        bps = round(bps)

        mb = (bps / 10 ** 6)
        if mb > 100:
            return f'{round(mb)} Mb/s'
        if mb >= 1:
            return f'{round(mb,2):.2f} Mb/s'
        kb = (bps / 10 ** 3)
        if kb >= 1:
            return f'{round(kb), 2} Kb/s'
        else:
            return f'{bps} b/s'

    def _bsum(self, perf):
        """
            Summarize all bps values
            for every interval (1 sec) of iperf testing

            >>> perf_summary('badvalue')
            'bad report formatting'
        """
        bsum = []
        try:
            intervals = perf.json['intervals']
            # ignoring tcp slowstart
            if len(intervals) > 3:
                intervals = perf.json['intervals'][2:]
            for interval in intervals:
                if not interval['sum']['omitted']:
                    bsum.append(interval['sum']['bits_per_second'])
        except Exception:
            raise

        if not bsum:
            raise KeyError(f'bad sum in perf results: {bsum}')

        return round(sum(bsum) / len(bsum), 2)

    def __repr__(self):
        return f"{type(self).__name__}"


class PerfTest(Perf):
    def __init__(self, config, queue):
        super().__init__(config, queue)
        self.direction = 'test'
        self.client.duration = 1

    def run(self):
        try:
            self.check_remote()
            self.result = self.client.run()
            if self.result:
                logging.debug(f'TEST perf connection complete')
                self.queue.put({'test': True})
            else:
                raise Exception('TEST connection incomplete')
        except Exception:
            raise


class PerfDown(Perf):
    def __init__(self, config, queue):
        super().__init__(config, queue)
        self.direction = 'download'
        self.client.reverse = True


class PerfUp(Perf):
    def __init__(self, config, queue):
        super().__init__(config, queue)
        self.direction = 'upload'
